/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.damain;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chanita
 */
public class readFile {
    
    private String brand ;
    private String domainname;
    private String dbname;
    private String dbtable;
    private String portnumber;
    private String username;
    private String password;
    
    
    
    public readFile(String brand){
        
        
        if(brand.equalsIgnoreCase("mysql")){
          
                Properties prope = new Properties();
                InputStream input;
            try {
                input = new FileInputStream("src/main/resource/DBmysql.properties");    
                prope.load(input);
                
                this.brand = prope.getProperty("BRAND");
                domainname = prope.getProperty("Domain_Name");
                dbname = prope.getProperty("DB_NAME");
                dbtable = prope.getProperty("DB_TABLE");
                portnumber = prope.getProperty("PORT_Number");
                username = prope.getProperty("User_Name");
                password = prope.getProperty("Password");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(readFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(readFile.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
                
        }
    }
        
       public String getbrand(){
            return brand;
        }
        
        public String getDomainName(){
            return domainname;
        }
        public String getdbname(){
            return dbname;
        }
        public String getdbtable(){
            return dbtable;
        }
        public String getportnumber(){
            return portnumber;
        }
        public String getusername(){
            return username;
        }
        public String getpassword(){
            return password;
        }
        
    
    
    
 
    
    
    
}
