/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.damain;

/**
 *
 * @author Room107
 */
public class Trip {
    private Airport fromAirport ;
    private Airport toAirport ;
    private byte noOfPassengers ;
    //private ZoneDateTime departureDateTime ;
   // private ZoneDateTime returnDateTime ;
    private boolean directFilght ;
    private TravelClass travelClass ;
    private TripType tripType;

    public void setTrip(Airport fromAirport,Airport toAirport,byte noOfPassengers,boolean directFilght,TravelClass travelClass,TripType tripType){
        this.fromAirport = fromAirport ;
        this.toAirport =   toAirport;
        this.noOfPassengers = noOfPassengers ;
        this.travelClass = travelClass;
        this.directFilght = directFilght;
        this.tripType = tripType ;   
    }
    
    public Airport getfromAirport(){
        return fromAirport;
    }
    public Airport gettoAirport(){
        return toAirport;
    }
    public byte getnoOfPassengers(){
        return noOfPassengers;
    }
    public boolean getdirectFilght(){
        return directFilght;
    }
    public TravelClass gettravelClass(){
        return travelClass;
    }
    public TripType gettripType(){
        return tripType;
    }

    
    }

