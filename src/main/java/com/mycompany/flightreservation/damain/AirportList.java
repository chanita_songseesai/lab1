/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.damain;

/**
 *
 * @author chanita
 */
public class AirportList {
    
    private String airline ;
    private String date;
    private String takeoof ;
    private String landing ;
    private String stop;
    private String price;
    
    
    
     public AirportList(String airline,String date,String takeoof,String landing,String stop,String price){
        
         this.airline = airline ;
         this.date = date;
         this.landing = landing;
         this.price = price;
         this.stop = stop;
         this.takeoof = takeoof;
     
    }
     
     public String getairline(){
        return airline;
       
     }
     public String getdate(){
         return date;
     }
     public String getlanding(){
         return landing;
     }
     public String getprice(){
         return price;
     }
     public String getstop(){
         return stop;
     }
    public String gettakeoof(){
        return takeoof;
    }
}
