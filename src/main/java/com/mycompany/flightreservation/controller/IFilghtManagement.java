/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import com.mycompany.flightreservation.damain.AirportList;
import com.mycompany.flightreservation.damain.Trip;
import java.util.ArrayList;


/**
 *
 * @author Room107
 */
public interface IFilghtManagement {
    
    public ArrayList<AirportList> searchFlight(Trip trip,boolean bestFace) throws MissingRequiredTripInfoException ;
}
