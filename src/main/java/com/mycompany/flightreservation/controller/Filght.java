/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import com.mycompany.flightreservation.damain.AirportList;
import com.mycompany.flightreservation.damain.Trip;
import com.mycompany.flightreservation.damain.readFile;
import factorymethod.DBConnection;
import factorymethod.DBConnectionFactory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Room107
 */
public class Filght implements IFilghtManagement {
   
    @Override
    public ArrayList<AirportList> searchFlight(Trip trip, boolean bestFace) throws MissingRequiredTripInfoException {
       ArrayList<AirportList> ar = new ArrayList<>();
       DBConnectionFactory dbfactory = new DBConnectionFactory();
       readFile file = new readFile("mysql");
       
       
       DBConnection dbconnection = dbfactory.createDBConnection("mysql",file.getDomainName(),file.getportnumber(),file.getdbname(),file.getusername(),file.getpassword());
       if(dbconnection != null){
           ResultSet st = dbconnection.excuteQuery("Select * from * flight ;");
           
           try {
               while (st.next()){
                   ar.add(new AirportList(st.getString(2),st.getString(3),st.getString(4),st.getString(5),st.getString(6),st.getString(7)));
               }
           } catch (SQLException ex) {
               Logger.getLogger(Filght.class.getName()).log(Level.SEVERE, null, ex);
           }    
       }else System.out.println("not found");
       
       return ar;
       
    }
    
}
