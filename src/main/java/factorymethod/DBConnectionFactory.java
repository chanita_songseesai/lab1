/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Room107
 */
public class DBConnectionFactory {
    
    
    public DBConnection createDBConnection(String brand , String host , String port , String dbName , String user , String password){
        if(brand.equalsIgnoreCase("mysql")){
            try {
                return new MySQLDBConnection(host,port,dbName,user,password);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else
        return null;
        return null;
    }
    
}
