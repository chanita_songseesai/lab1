/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Room107
 */
public class MySQLDBConnection implements DBConnection {
    
    private final String HOST = null ;
    private final String USERNAME = null ;
    private final String PASSWORD = null ;
    private final String PORT = null ;
    private final String CONNECTION = null ;
    private Connection connection ;
    
    public  MySQLDBConnection(String host , String dbName , String user , String password , String port) throws ClassNotFoundException {
        
            try {
                Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://"
                    + host + ":" + port + "/" + dbName ;
                connection = DriverManager.getConnection(
                        url , user , password);
            } catch (SQLException ex) {
                Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
                
            }
        }

    @Override
    public ResultSet excuteQuery(String query) {
        try {
            
            Statement st = connection.createStatement();
            ResultSet results = st.executeQuery(query);
            return results;
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
        
        
        
    }

    @Override
    public void Update(String command) {
        
    
    
    try {
            Statement st = connection.createStatement();
            st.executeUpdate(command);
        
        } catch (SQLException ex){
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE , null , ex);
        }
    
    
    
    }

    @Override
    public void close() {
      if(connection != null ){
   
    try 
        {
            connection.close();
        
    }catch (SQLException ex){
        Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null , ex);
    }
    
    
    }
    
    
    }
        
        
        
    }

    
    
    
    

